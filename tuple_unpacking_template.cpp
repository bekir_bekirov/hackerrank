#include <iostream>
#include <tuple>


// template<size_t I = 0, typename... Args>
// typename std::enable_if<I == sizeof...(Args), void>::type test_tuple_func(std::tuple<Args...> &&arg) {}

// template<size_t I = 0, typename... Args>
// typename std::enable_if<I < sizeof...(Args), void>::type test_tuple_func(std::tuple<Args...> &&arg)
// {
// 	std::cout << std::get<I>(std::forward<std::tuple<Args...> >(arg)) << std::endl;
// 	test_tuple_func<I + 1, Args...>(std::forward<std::tuple<Args...> >(arg));
// }

// template<size_t I = 0, typename... Args>
// void test_tuple_func(std::tuple<Args...> &&arg) {}

template<size_t I = 0, typename... Args>
void test_tuple_func(std::tuple<Args...> &&arg)
{
	if constexpr (I < sizeof...(Args)) {
        std::cout << std::get<I>(std::forward<std::tuple<Args...> >(arg)) << std::endl;
	    test_tuple_func<I + 1, Args...>(std::forward<std::tuple<Args...> >(arg));
    }
}

int main(int args, char **argv)
{
    // auto tuple_instance = std::make_tuple(1, 3.4, "hello!");

    test_tuple_func(std::make_tuple(1, 3.4, "hello!"));
    
    return EXIT_SUCCESS;
}
