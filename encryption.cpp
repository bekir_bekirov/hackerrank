#include <bits/stdc++.h>
#include <algorithm>

using namespace std;

/*
 * Complete the 'encryption' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts STRING s as parameter.
 */

string encryption(string s) {
    vector<string> matrix_string;

    s.erase(remove(s.begin(), s.end(), ' '), s.end());
    cout << s << endl;
    size_t floor_size = floor(pow(s.size(), 0.5f));
    size_t ceil_size = ceil(pow(s.size(), 0.5f));

    if (floor_size * ceil_size < s.size()) ++floor_size;

    // cout << floor_size << ceil_size << endl;

    // matrix_string.resize(floor_size);
    size_t src_pos = 0;
    size_t dst_pos = 0;

    for (size_t i = 0; i < floor_size; ++i) {
        string buf_str;
        // buf_str.insert(dst_pos, s, src_pos, ceil_size);
        buf_str = s.substr(src_pos, ceil_size);
        if (!buf_str.empty()) matrix_string.push_back(buf_str);
        src_pos += ceil_size;
        // cout <<buf_str << endl;
    }

    string res_str;
    for (size_t i = 0; i < ceil_size; ++i) {
        for (auto &str : matrix_string) {
            if (str[i] != 0) res_str += str[i];  
        }
        res_str += ' ';
    }

    // cout << res_str << endl;
    
    return res_str;
}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s;
    getline(cin, s);

    string result = encryption(s);

    fout << result << "\n";

    fout.close();

    return 0;
}
