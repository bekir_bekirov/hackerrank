#include <iostream>
using namespace std;

// Enter your code for reversed_binary_value<bool...>()

#include <vector>


template <bool... Args>
int reversed_binary_value()
{
    vector<bool> vec{Args...};
    size_t i = 0;
    int value = 0;

    for( auto el : vec) {
        value |= (el << i);
        ++i;
    }

    return value;
};

template <int n, bool...digits>
struct CheckValues {
  	static void check(int x, int y)
  	{
    	CheckValues<n-1, 0, digits...>::check(x, y);
    	CheckValues<n-1, 1, digits...>::check(x, y);
  	}
};

template <bool...digits>
struct CheckValues<0, digits...> {
  	static void check(int x, int y)
  	{
    	int z = reversed_binary_value<digits...>();
    	std::cout << (z+64*y==x);
  	}
};

int main()
{
  	int t; std::cin >> t;

  	for (int i=0; i!=t; ++i) {
		int x, y;
    	cin >> x >> y;
    	CheckValues<6>::check(x, y);
    	cout << "\n";
  	}

    // int a = reversed_binary_value<true, false, false, true>();
    // cout << a << endl;
}
