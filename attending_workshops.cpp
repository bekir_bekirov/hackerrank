#include<bits/stdc++.h>

using namespace std;

//Define the structs Workshops and Available_Workshops.
//Implement the functions initialize and CalculateMaxWorkshops

struct Workshops
{
    int start_time = 0;
    int duration = 0;
    int end_time = 0;;
};

struct Available_Workshops
{
    int n = 0;
    Workshops *array_workshops = nullptr;

    Available_Workshops(int *start_time, int *duration, int num) : n(num)
    {
        array_workshops = new Workshops[num];

        for (size_t n = 0; n < num; ++n) {
        array_workshops[n].start_time = start_time[n];
        array_workshops[n].duration = duration[n];
        array_workshops[n].end_time = array_workshops[n].start_time + array_workshops[n].duration;
        }
    }

    ~Available_Workshops() 
    {
        delete[] array_workshops;
        array_workshops = nullptr;
    }
};

Available_Workshops *initialize(int *start_time, int *duration, int num)
{
    static Available_Workshops workshops(start_time, duration, num);
    
    for (size_t n = 0; n < num; ++n) {
        workshops.array_workshops[n].start_time = start_time[n];
        workshops.array_workshops[n].duration = duration[n];
        workshops.array_workshops[n].end_time = workshops.array_workshops[n].start_time +
                        workshops.array_workshops[n].duration;
    }

    return &workshops;
}

size_t CalculateMaxWorkshops(Available_Workshops *ptr)
{
    size_t cnt = 1;

    sort(ptr->array_workshops,(ptr->array_workshops)+ptr->n,[](Workshops &a,Workshops &b){return a.end_time < b.end_time;});

    Workshops  *buf_workshop = ptr->array_workshops;
    for (size_t n = 1; n < ptr->n; ++n) {
        if (ptr->array_workshops[n].start_time >= buf_workshop->end_time) {
            buf_workshop = ptr->array_workshops + n;
            ++cnt;
        }
    }

    return cnt;
}

int main(int argc, char *argv[]) {
    int n; // number of workshops
    cin >> n;
    // create arrays of unknown size n
    int* start_time = new int[n];
    int* duration = new int[n];

    for(int i=0; i < n; i++){
        cin >> start_time[i];
    }
    for(int i = 0; i < n; i++){
        cin >> duration[i];
    }

    Available_Workshops * ptr;
    ptr = initialize(start_time,duration, n);
    cout << CalculateMaxWorkshops(ptr) << endl;
    return 0;
}
