#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <set>
#include <cassert>
using namespace std;

struct Node{
   Node* next;
   Node* prev;
   int value;
   int key;
   Node(Node* p, Node* n, int k, int val):prev(p),next(n),key(k),value(val){};
   Node(int k, int val):prev(NULL),next(NULL),key(k),value(val){};
};

class Cache{
   
   protected: 
   map<int,Node*> mp; //map the key to the node in the linked list
   int cp;  //capacity
   Node* tail; // double linked list tail pointer
   Node* head; // double linked list head pointer
   virtual void set(int, int) = 0; //set function
   virtual int get(int) = 0; //get function
   virtual ~Cache() = default;
};

class LRUCache : public Cache
{
public:
    LRUCache(int capacity)
    {
        cp = capacity;
        tail = head = nullptr;
    }

    ~LRUCache() = default;

    void set(int key, int value) override
    {
        auto it = mp.find(key);
        if (it != mp.end()) {
            it->second->value = value;
            return;
        }
        
        if(mp.size() >= cp)
        {
            tail = tail->next;
            mp.erase(tail->prev->key);
            delete tail->prev; 
            tail->prev = nullptr;
        }
        
        Node *item = new Node(key, value);
        mp[key] = item;
        
        if (tail == head && tail == nullptr) {
            tail = head = item;
            item->next = item;
            item->prev = item;
            return;
        }

        if (tail == head) {
            head->next = item;
            item->prev = head;
            head = item;
            return;
        }

        if (tail != head) {
            head->next = item;
            item->prev = head;
            head = item;
            return;
        }
    }

    int get(int key) override
    {
        int value = -1; 
        Node *item = nullptr;
        
        auto it = mp.find(key);
        if (it != mp.end()) {
            item = mp[key];
            value = item->value;
            return value;
        }
        
        return -1;

        // if (item) {
        //     if (item == tail) {
        //         // cout << "item " << item << endl;
        //         // cout << "value " << item->value << endl;
        //         value = item->value;
        //         tail = tail->next;
        //         tail->prev = nullptr;
        //         delete item;
        //         --cnt;
        //         return value;
        //     }

        //     if (item == head) {
        //         value = item->value;
        //         head = head->prev;
        //         head->next = nullptr;
        //         delete item;
        //         --cnt;
        //         return value;
        //     }

        //     if (tail != head) {
        //         item->prev->next = item->next;
        //         value = item->value;
        //         delete item;
        //         --cnt;
        //         return value;
        //     } else {
        //         if (tail != nullptr) {
        //             value = item->value;
        //             delete item;
        //             tail = head = nullptr;
        //             --cnt;
        //             return value;
        //         } else return value;
        //     }

        // } else return value;
    }
};

int main() {
   int n, capacity,i;
   cin >> n >> capacity;
   LRUCache l(capacity);
   for(i=0;i<n;i++) {
      string command;
      cin >> command;
      if(command == "get") {
         int key;
         cin >> key;
         cout << l.get(key) << endl;
      } 
      else if(command == "set") {
         int key, value;
         cin >> key >> value;
         l.set(key,value);
      }
   }
   return 0;
}
