#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;

/*Write the class AddElements here*/

// template<typename T>
// class AddElements
// {
// public:
//     AddElements(T a) : a{a} {}

//     T add(T b)
//     {
//         return a + b;
//     }

// private:
//     T a;
// };

// template<>
// class AddElements<string>
// {
// public:
//     AddElements(const string &str) : a{str}
//     {

//     }
    
//     string &concatenate(const string &b)
//     {
//         return a.append(b);
//     }

// private:
//     string a;
// };

// int start_up() {
//     ios_base::sync_with_stdio(false);
//     cin.tie(NULL);
//     return 0;
// }

// int static r = start_up();

// #define endl '\n';

template< typename T>
class AddElements
{
public:
    AddElements(T a) : a{a} {}

    template<typename U = T>
    typename enable_if<is_arithmetic<U>::value, U>::type add(U b)
    {
        return a + b;
    }

    template<typename U = T>
    typename enable_if<!is_arithmetic<U>::value, U>::type concatenate(U b)
    {
        return a + b;
    }

private:
    T a;
};

int main () {
  int n,i;
  cin >> n;
  for(i=0;i<n;i++) {
    string type;
    cin >> type;
    if(type=="float") {
        double element1,element2;
        cin >> element1 >> element2;
        AddElements<double> myfloat (element1);
        cout << myfloat.add(element2) << endl;
    }
    else if(type == "int") {
        int element1, element2;
        cin >> element1 >> element2;
        AddElements<int> myint (element1);
        cout << myint.add(element2) << endl;
    }
    else if(type == "string") {
        string element1, element2;
        cin >> element1 >> element2;
        AddElements<string> mystring (element1);
        cout << mystring.concatenate(element2) << endl;
    }
  }
  return 0;
}
