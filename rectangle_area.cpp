#include <iostream>

using namespace std;
/*
 * Create classes Rectangle and RectangleArea
 */

class Rectangle
{
public:
    Rectangle() {}
    virtual ~Rectangle() {}
    virtual void display(void) const
    {
        cout << width << " " << height << endl;
    }

    int GetWidth(void) const
    {
        return width;
    }

    int GetHeight(void) const
    {
        return height;
    }

    void SetWidth(int w)
    {
        width = w;
    }

    void SetHeight(int h)
    {
        height = h;
    }

private:
    int height;
    int width;

};

class RectangleArea : public Rectangle
{
public:
    RectangleArea() : Rectangle() {}
    void read_input(void)
    {
        int w, h;

        cin >> w >> h; 
        SetWidth(w);
        SetHeight(h);
    }
    
    void display(void) const override
    {
        cout << GetWidth() * GetHeight() << endl;
    }
};

int main()
{
    /*
     * Declare a RectangleArea object
     */
    RectangleArea r_area;
    
    /*
     * Read the width and height
     */
    r_area.read_input();
    
    /*
     * Print the width and height
     */
    r_area.Rectangle::display();
    
    /*
     * Print the area
     */
    r_area.display();
    
    return 0;
}