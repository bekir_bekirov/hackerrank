#include <iostream>
#include <vector>
#include <string>
#include <functional>
using namespace std;

class Spell { 
    private:
        string scrollName;
    public:
        Spell(): scrollName("") { }
        Spell(string name): scrollName(name) { }
        virtual ~Spell() { }
        string revealScrollName() {
            return scrollName;
        }
};

class Fireball : public Spell { 
    private: int power;
    public:
        Fireball(int power): power(power) { }
        void revealFirepower(){
            cout << "Fireball: " << power << endl;
        }
};

class Frostbite : public Spell {
    private: int power;
    public:
        Frostbite(int power): power(power) { }
        void revealFrostpower(){
            cout << "Frostbite: " << power << endl;
        }
};

class Thunderstorm : public Spell { 
    private: int power;
    public:
        Thunderstorm(int power): power(power) { }
        void revealThunderpower(){
            cout << "Thunderstorm: " << power << endl;
        }
};

class Waterbolt : public Spell { 
    private: int power;
    public:
        Waterbolt(int power): power(power) { }
        void revealWaterpower(){
            cout << "Waterbolt: " << power << endl;
        }
};

class SpellJournal {
    public:
        static string journal;
        static string read() {
            return journal;
        }
}; 
string SpellJournal::journal = "";

void counterspell(Spell *spell) {

/* Enter your code here. Read input from STDIN. Print output to STDOUT */
    size_t cnt = 0;

    if (Fireball *fireball = dynamic_cast<Fireball *>(spell)) {
        fireball->revealFirepower();
    } else if (Frostbite *frostbite = dynamic_cast<Frostbite *>(spell)) {
        frostbite->revealFrostpower();  
    } else if (Waterbolt *waterbolt = dynamic_cast<Waterbolt *>(spell)) {
        waterbolt->revealWaterpower();
    } else if (Thunderstorm *thunderstorm = dynamic_cast<Thunderstorm *>(spell)) {
        thunderstorm->revealThunderpower();
    } else {
        string spells_journal = SpellJournal::read();
        string spells_string = spell->revealScrollName();
        
        function<int(const string&, const string&, int, int)> lcs;

        // using lcs_t = int(*)(const string &X, const string &Y, int m, int n);

        lcs = [&] (const string &X, const string &Y, int m, int n) -> int {
            if (m == 0 || n == 0) return 0;
            if (X[m - 1] == Y[n - 1]) return 1 + lcs(X, Y, m - 1, n - 1);
            else return max(lcs(X, Y, m, n - 1), lcs(X, Y, m - 1, n));
        };

        cnt = lcs(spells_journal, spells_string, spells_journal.size(), spells_string.size());

        cout << cnt << endl;
    }
}

class Wizard {
    public:
        Spell *cast() {
            Spell *spell;
            string s; cin >> s;
            int power; cin >> power;
            if(s == "fire") {
                spell = new Fireball(power);
            }
            else if(s == "frost") {
                spell = new Frostbite(power);
            }
            else if(s == "water") {
                spell = new Waterbolt(power);
            }
            else if(s == "thunder") {
                spell = new Thunderstorm(power);
            } 
            else {
                spell = new Spell(s);
                cin >> SpellJournal::journal;
            }
            return spell;
        }
};

int main() {
    int T;
    cin >> T;
    Wizard Arawn;
    while(T--) {
        Spell *spell = Arawn.cast();
        counterspell(spell);
    }
    return 0;
}
