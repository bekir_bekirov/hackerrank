#include <iostream>
#include <deque> 
using namespace std;

#include <algorithm>
#include <iterator>

// void printKMax(int arr[], int n, int k){
// 	//Write your code here.
//     deque<int> buf_compare(arr, arr + n);

//     auto it_begin = buf_compare.begin();
//     for (size_t i = 0; i < n; ++i) {
//         auto it_end = it_begin;
//         advance(it_end, k);
//         auto it_max = max_element(it_begin, it_end);
//         ++it_begin;
//         cout << *it_max << " ";
        
//         if (it_end == buf_compare.end()) break;
//     }
//     cout << endl;
// }

// void printKMax(int arr[], int n, int k){
// 	//Write your code here.
//     deque<int> buf_compare;
//     int max_el;

//     for (size_t j = 0; j <= n - k; ++j) {
//         buf_compare.push_back(arr[j]);
//         max_el = buf_compare[j];

//         for (size_t i = 1; i < k; ++i) {
//             buf_compare.push_back(arr[j + i]);
//             max_el = max(max_el, buf_compare.back());
//         }
//         cout << max_el << " ";
//         buf_compare.pop_front();
//     }
//     cout << endl;
// }

void printKMax(int arr[], int n, int k)
{
    std::deque<int> buf_compare(k);

    int i;
    for (i = 0; i < k; ++i) {
        while ((!buf_compare.empty()) && arr[i] >= 
                            arr[buf_compare.back()])
            buf_compare.pop_back();
        buf_compare.push_back(i);
    }
    for (; i < n; ++i) {
        cout << arr[buf_compare.front()] << " ";

        while ((!buf_compare.empty()) && buf_compare.front() <= 
                                           i - k)
            buf_compare.pop_front(); 

        while ((!buf_compare.empty()) && arr[i] >= 
                             arr[buf_compare.back()])
            buf_compare.pop_back();

        buf_compare.push_back(i);
    }
    cout << arr[buf_compare.front()];
}

int main(){
  
	int t;
	cin >> t;
	while(t>0) {
		int n,k;
    	cin >> n >> k;
    	int i;
    	int arr[n];
    	for(i=0;i<n;i++)
      		cin >> arr[i];
    	printKMax(arr, n, k);
    	t--;
  	}
  	return 0;
}