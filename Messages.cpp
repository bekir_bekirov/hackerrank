#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

class Message {
public: 
    Message() : msg_id(0) {}
    const string& get_text() {
        return msg_text;
    }

    friend bool operator< (const Message &lhs, const Message &rhs);

    size_t msg_id;
    string msg_text;
};

bool operator< (const Message &lhs, const Message &rhs)
{
    return (lhs.msg_id < rhs.msg_id) ? true : false;
}

class MessageFactory {
public:
    MessageFactory() : msg_id(0) {}
    Message create_message(const string& text) {
        Message msg;

        msg.msg_id = msg_id;
        msg.msg_text = text;
        msg_storage.push_back(msg);
        ++msg_id;

        return msg;
    }
private:
    size_t msg_id;
    vector<Message> msg_storage;
};

class Recipient {
public:
    Recipient() {}
    void receive(const Message& msg) {
        messages_.push_back(msg);
    }
    void print_messages() {
        fix_order();
        for (auto& msg : messages_) {
            cout << msg.get_text() << endl;
        }
        messages_.clear();
    }
private:
    void fix_order() {
        sort(messages_.begin(), messages_.end());
    }
    vector<Message> messages_;
};

class Network {
public:
    static void send_messages(vector<Message> messages, Recipient& recipient) {
    // simulates the unpredictable network, where sent messages might arrive in unspecified order
        random_shuffle(messages.begin(), messages.end());         
        for (auto msg : messages) {
            recipient.receive(msg);
        }
    }
};

int main() {
    MessageFactory message_factory;
    Recipient recipient;
    vector<Message> messages;
    string text;
    while (getline(cin, text)) {
        messages.push_back(message_factory.create_message(text));
    }
    Network::send_messages(messages, recipient);
    recipient.print_messages();
}
